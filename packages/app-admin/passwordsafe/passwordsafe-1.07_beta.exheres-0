# Copyright 2017 Tom Briden
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'passwordsafe-1.02.1.ebuild' from Gentoo, which is:
#     Copyright 1999-2017 Gentoo Foundation

MYPV="${PV/_beta/BETA}"
require github [ user=pwsafe project=pwsafe tag=${MYPV} ]
require cmake [ api=2 ]
require gtk-icon-cache

SUMMARY="popular secure and convenient password manager"
HOMEPAGE="https://pwsafe.org"

LICENCES="Artistic-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    qr          [[ description = [ Show password as QR code ] ]]
    xml         [[ description = [ Enable support for importing from XML ] ]]
    yubikey     [[ description = [ Enable support for YubiKey 2FA ] ]]
"

DEPENDENCIES="
    build:
        sys-apps/util-linux
        x11-libs/libXt
        x11-libs/libXtst
        x11-libs/wxGTK:3.0
        qr? ( media-libs/qrencode:0 )
        yubikey? ( sys-auth/yubikey-personalization )
        xml? ( dev-libs/xerces-c )
    build+run:
        app-arch/zip
        sys-devel/gettext
    build+test:
        dev-cpp/gtest
"

BUGS_TO="tom@decompile.me.uk"

CMAKE_SRC_CONFIGURE_TESTS=(
  '-DNO_GTEST:BOOL=FALSE -DNO_GTEST:BOOL=TRUE'
)

CMAKE_SRC_CONFIGURE_OPTIONS=(
  '!qr NO_QR'
  'xml XML_XERCESC'
  '!yubikey NO_YUBI'
)

#they've started git cloning gtest during configure rather than using install
RESTRICT="test"

MYWORK="${WORKBASE}"/pwsafe-"${MYPV}"
src_prepare() {
    default

    # binary name pwsafe is in use by app-crypt/pwsafe, we use passwordsafe
    # instead. Perform required changes in linking files
    edo sed -i "${MYWORK}"/install/desktop/pwsafe.desktop -e "s/pwsafe/${PN}/g"
    edo sed -i "${MYWORK}"/docs/pwsafe.1 \
        -e 's/PWSAFE/PASSWORDSAFE/' \
        -e "s/^.B pwsafe/.B ${PN}/"
}

src_install() {
    BUILD_DIR=${WORKBASE}/build

    edo pushd "${BUILD_DIR}"
    newbin ${BUILD_DIR}/pwsafe ${PN}

    insinto /usr/share/locale
    doins -r ${BUILD_DIR}/src/ui/wxWidgets/I18N/mos/*

    insinto /usr/share/${PN}/help
    doins "${BUILD_DIR}"/help/*.zip

    edo popd

    newman "${MYWORK}"/docs/pwsafe.1 ${PN}.1

    dodoc "${MYWORK}"/README.md "${MYWORK}"/docs/{ReleaseNotes.txt,ChangeLog.txt}

    insinto /usr/share/pwsafe/xml
    doins "${MYWORK}"/xml/*

    insinto /usr/share/pixmaps/
    newins "${MYWORK}"/install/graphics/pwsafe.png ${PN}.png

    insinto /usr/share/applications
    newins "${MYWORK}"/install/desktop/pwsafe.desktop ${PN}.desktop

}

