# Copyright 2017 Tom Briden
# Distributed under the terms of the GNU General Public License v2

require github [ user=mrash ] \
        autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 ] ] \
        systemd-service

SUMMARY="Single Packet Authorization > Port Knocking"
DESCRIPTION="
fwknop stands for the 'FireWall KNock OPerator', and implements an authorization
scheme called Single Packet Authorization (SPA). This method of authorization is
based around a default-drop packet filter (fwknop supports iptables and
firewalld on Linux, ipfw on FreeBSD and Mac OS X, and PF on OpenBSD) and
libpcap. SPA is essentially next generation port knocking
"
HOMEPAGE="http://www.cipherdyne.org/fwknop/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
	client    [[ description = [ build client application ] ]]
	gpg       [[ description = [ build support for GPG encryption ] ]]
	iptables  [[ description = [ build with iptables support ] ]]
	nfqueue   [[ description = [ enable Netfilter Queue server mode, no libcap dependency ] ]]
	server    [[ description = [ build server application ] ]]
"

DEPENDENCIES="
    build:
        gpg? (
            app-crypt/gpgme
            dev-libs/libassuan
            dev-libs/libgpg-error
        )
        iptables? ( net-firewall/iptables )
        nfqueue? ( net-libs/libnetfilter_queue )
        server? ( !nfqueue? ( !udp-server? ( net-libs/libpcap ) ) )
    build+run:
"

BUGS_TO="tom@decompile.me.uk"

DOCS=( AUTHORS ChangeLog README.md )

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
  "iptables iptables /usr/sbin/iptables"
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
	'server'
	'client'
	'gpg gpgme'
    'nfqueue nfq_capture'
)

src_install() {
    default
    if option server ; then
        install_systemd_files
    fi
}

