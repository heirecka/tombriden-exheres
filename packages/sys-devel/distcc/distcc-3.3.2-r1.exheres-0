# Copyright 2017 Tom Briden <tom@decompile.me.uk>
# Based in part on original exheres which is
#   Copyright 2009 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2

AT_M4DIR="m4"

require github [ user=distcc tag=v${PV/_rc/rc} ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
require python [ blacklist=2 multibuild=false ]
require systemd-service

SUMMARY="distcc: a fast, free distributed C/C++ compiler"
DESCRIPTION="
distcc is a program to distribute builds of C, C++, Objective C
or Objective C++ code across several machines on a network.
distcc should always generate the same results as a local build,
is simple to install and use, and is usually much faster than a local compile.
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="avahi gnome gtk
    gnome [[ requires = [ gtk ] ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        dev-libs/popt
        user/distcc
        group/distcc
        avahi? ( net-dns/avahi )
        gtk? (
            x11-libs/gtk+:2
            x11-libs/pango
            gnome? (
                gnome-platform/libglade:2
                gnome-platform/libgnome:2
                gnome-platform/libgnomeui:2
            )
        )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}/${PN}-detect_more_compilers.patch"
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-Werror
    --enable-rfc2553
    --with-docdir=/usr/share/doc/${PNV}
    --libexecdir=/usr/$(exhost --target)/lib/dhcpcd
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    avahi
    gnome
    gtk
)

pkg_setup() {
    exdirectory --allow /run
}

src_install() {
    default
    python_bytecompile

    dodir /usr/$(exhost --target)/lib/distcc
    insinto /usr/$(exhost --target)/lib/distcc
    local f
    for f in {${CHOST}-,}{gcc,g++,c++}; do
        dosym /usr/bin/distcc /usr/$(exhost --target)/lib/distcc/${f}
    done

    keepdir /run/distccd
    edo chown distcc:distcc "${IMAGE}"/run/distccd
    if option providers:eudev ; then
        newinitd "${FILES}/${PN}-initd" distccd
        newconfd "${FILES}/${PN}-confd" distccd
    elif option providers:systemd ; then
        install_systemd_files
        insinto /etc/conf.d
        doins "${FILES}"/distccd.conf
    fi
}

